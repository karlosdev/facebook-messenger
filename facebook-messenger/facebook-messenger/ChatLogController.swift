//
//  ChatLogController.swift
//  facebook-messenger
//
//  Created by Karlos Aguirre on 10/31/18.
//  Copyright © 2018 Karlos Aguirre. All rights reserved.
//

import UIKit
import CoreData

class ChatLogController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    private let cellId = "cellId"
    var friend:Friend? {
        didSet {
            navigationItem.title = friend?.name
            messages = friend?.message?.allObjects as? [Message]
            
            messages = messages?.sorted(by: { (a, b) -> Bool in
                return a.date?.compare(b.date!) == .orderedAscending
            })
        }
    }
    
    var messages:[Message]?
    
    let messageInputContainerView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        return view
    }()
    
    let inputTextField: UITextField = {
        let textField = UITextField()
        textField.placeholder = "Enter a message..."
        return textField
    }()
    
    lazy var sendButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Send", for: .normal)
        let tintColor = UIColor(red: 0, green: 137/255, blue: 249/255, alpha: 1)
        button.setTitleColor(tintColor, for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        button.addTarget(self, action: #selector(handleSend), for: .touchUpInside)
        return button
    }()
    
    @objc func handleSend() {
        print("handle send text")
        
        let delegate = UIApplication.shared.delegate as? AppDelegate
        if let managedContext = delegate?.persistentContainer.viewContext {
            let newMessage = FriendsController.createMessage(with: inputTextField.text!, friend: friend!, minsAgo: 0, context: managedContext, isSender: true)
            do {
                try managedContext.save()
                messages?.append(newMessage)
                let item = messages!.count - 1
                let insertionIndexPath = IndexPath(item: item, section: 0)
                collectionView.insertItems(at: [insertionIndexPath])
                collectionView.scrollToItem(at: insertionIndexPath, at: .bottom, animated: true)
                inputTextField.text = nil
                
            } catch let err {
                print(err)
            }
        }
    }
    
    var bottomConstraint:NSLayoutConstraint?
    
    @objc func simulate() {
        
        let delegate = UIApplication.shared.delegate as? AppDelegate
        if let managedContext = delegate?.persistentContainer.viewContext {
            let message = FriendsController.createMessage(with: "Here's a text message that was sent a few minutes ago...", friend: friend!, minsAgo: 1, context: managedContext)
            do {
                try managedContext.save()
                messages?.append(message)
                
                messages = messages?.sorted(by: { (message1, message2) -> Bool in
                    message1.date?.compare(message2.date!) == .orderedAscending
                })
                if let item = messages?.lastIndex(of: message) {
                    let receivingIndexPath = IndexPath(item: item, section: 0)
                    collectionView.insertItems(at: [receivingIndexPath])
                    inputTextField.text = nil
                }
            } catch let err {
                print(err)
            }
        }
    }
    
    let fetchedResultsController:NSFetchedResultsController = {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Message")
        let delegate = UIApplication.shared.delegate as? AppDelegate
        if let managedContext = delegate?.persistentContainer.viewContext {
            let frc = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: managedContext, sectionNameKeyPath: <#T##String?#>, cacheName: <#T##String?#>)
            return frc
        }
        return NSFetchedResultsController()
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Simulate", style: UIBarButtonItem.Style.plain, target: self, action: #selector(simulate))
        
        tabBarController?.tabBar.isHidden = true
        collectionView.backgroundColor = .white
        
        collectionView.register(ChatLogMessageCell.self, forCellWithReuseIdentifier: cellId)
        view.addSubview(messageInputContainerView)
        view.addoConstraintWithFormat("H:|[v0]|", views: messageInputContainerView)
        view.addoConstraintWithFormat("V:[v0(48)]", views: messageInputContainerView)
        
        bottomConstraint = NSLayoutConstraint(item: messageInputContainerView, attribute: .bottom, relatedBy: .equal, toItem: view, attribute: .bottom, multiplier: 1, constant: 0)
        view.addConstraint(bottomConstraint!)
        
        setupInputComponents()
        
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardNotification), name: UIResponder.keyboardWillShowNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardNotification), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func handleKeyboardNotification(notification:NSNotification) {
        
        if let userInfo = notification.userInfo {
            
            let isKeyboardShowing = notification.name == UIResponder.keyboardWillShowNotification
            let frame = (userInfo[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue
            if let height = frame?.height {
                bottomConstraint?.constant = isKeyboardShowing ? -height : 0
                
                UIView.animate(withDuration: 0, delay: 0, options: UIView.AnimationOptions.curveEaseOut, animations: {
                    self.view.layoutIfNeeded()
                }) { (completed) in
                    
                    if isKeyboardShowing, let count = self.messages?.count {
                        let indexPath = IndexPath(row: count - 1, section: 0)
                        self.collectionView.scrollToItem(at: indexPath, at: UICollectionView.ScrollPosition.bottom, animated: true)
                    }
                }
            }
            
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        inputTextField.endEditing(true)
    }
    
    func setupInputComponents() {
        let borderView = UIView()
        borderView.backgroundColor = UIColor(white: 0.5, alpha: 0.5)
        
        messageInputContainerView.addSubview(inputTextField)
        messageInputContainerView.addSubview(sendButton)
        messageInputContainerView.addSubview(borderView)
        
        messageInputContainerView.addoConstraintWithFormat("H:|-8-[v0][v1(60)]|", views: inputTextField, sendButton)
        messageInputContainerView.addoConstraintWithFormat("V:|[v0]|", views: inputTextField)
        messageInputContainerView.addoConstraintWithFormat("V:|[v0]|", views: sendButton)
        
        messageInputContainerView.addoConstraintWithFormat("H:|[v0]|", views: borderView)
        messageInputContainerView.addoConstraintWithFormat("V:|[v0(0.5)]", views: borderView)
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let count = messages?.count {
            return count
        }
        return 0
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! ChatLogMessageCell
        cell.messageTextView.text = messages?[indexPath.item].text
        
        if let message = messages?[indexPath.item],  let messageText = message.text, let profileImageName = message.friend?.profileImageName {
            
            cell.profileImageView.image = UIImage(named: profileImageName)
            
            let size = CGSize(width: 250, height: 1000)
            let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
            let estimatedFrame = NSString(string: messageText).boundingRect(with: size, options: options, attributes: [kCTFontAttributeName as NSAttributedString.Key: UIFont.systemFont(ofSize: 18)], context: nil)
            
            if !message.isSender {
                cell.messageTextView.frame = CGRect(x:48 + 8, y: 0, width: estimatedFrame.width + 16, height: estimatedFrame.height + 20)
                cell.textBubbleView.frame = CGRect(x:48 - 10, y: -4, width: estimatedFrame.width + 16 + 8 + 16, height: estimatedFrame.height + 20 + 6)
                cell.profileImageView.isHidden = false
                cell.bubbleImageView.tintColor = UIColor(white: 0.95, alpha: 1)
                cell.messageTextView.textColor = .black
                
                cell.bubbleImageView.image = ChatLogMessageCell.grayBubbleImage
            } else {
                
                //outgoing sending message
                cell.messageTextView.frame = CGRect(x:view.frame.width - estimatedFrame.width - 16 - 16 - 8, y: 0, width: estimatedFrame.width + 16, height: estimatedFrame.height + 20)
                cell.textBubbleView.frame = CGRect(x: view.frame.width - estimatedFrame.width - 16 - 8 - 16 - 10, y: -4, width: estimatedFrame.width + 16 + 8 + 10, height: estimatedFrame.height + 20 + 6)
                cell.profileImageView.isHidden = true
                cell.bubbleImageView.tintColor = UIColor(red: 0, green: 137/255, blue: 249/255, alpha: 1)
                cell.messageTextView.textColor = .white
                
                cell.bubbleImageView.image = ChatLogMessageCell.blueBubbleImage
            }
            
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if let messageText = messages?[indexPath.item].text {
            let size = CGSize(width: 250, height: 1000)
            let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
            let estimatedFrame = NSString(string: messageText).boundingRect(with: size, options: options, attributes: [kCTFontAttributeName as NSAttributedString.Key: UIFont.systemFont(ofSize: 18)], context: nil)
            return CGSize(width: view.frame.width, height: estimatedFrame.height + 20)
        }
        return CGSize(width: view.frame.width, height: 100)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 8, left: 0, bottom: 0, right: 0)
    }
}

class ChatLogMessageCell: BaseCell {
    
    let messageTextView: UITextView = {
        let textView = UITextView()
        textView.font = UIFont.systemFont(ofSize: 16)
        textView.text = "Sample message"
        textView.backgroundColor = UIColor.clear
        return textView
    }()
    
    let textBubbleView: UIView = {
        let view = UIView()
        //view.backgroundColor = UIColor(white: 0.95, alpha: 1)
        view.layer.cornerRadius = 15
        view.layer.masksToBounds = true
        return view
    }()
    
    let profileImageView:UIImageView =  {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.layer.cornerRadius = 15
        imageView.layer.masksToBounds = true
        return imageView
    }()
    
    static let grayBubbleImage = UIImage(named: "bubble_gray")?.resizableImage(withCapInsets: UIEdgeInsets(top: 22, left: 26, bottom: 22, right: 26)).withRenderingMode(.alwaysTemplate)
    static let blueBubbleImage = UIImage(named: "bubble_blue")?.resizableImage(withCapInsets: UIEdgeInsets(top: 22, left: 26, bottom: 22, right: 26)).withRenderingMode(.alwaysTemplate)
    
    let bubbleImageView:UIImageView = {
        let imageView = UIImageView()
        imageView.image = grayBubbleImage
        imageView.tintColor = UIColor(white: 0.95, alpha: 1)
        return imageView
    }()
    
    override func setupViews() {
        super.setupViews()
        
        addSubview(textBubbleView)
        addSubview(messageTextView)
        addSubview(profileImageView)
        addoConstraintWithFormat("H:|-8-[v0(30)]", views: profileImageView)
        addoConstraintWithFormat("V:[v0(30)]|", views: profileImageView)
        profileImageView.backgroundColor = .red
        
        textBubbleView.addSubview(bubbleImageView)
        textBubbleView.addoConstraintWithFormat("H:|[v0]|", views: bubbleImageView)
        textBubbleView.addoConstraintWithFormat("V:|[v0]|", views: bubbleImageView)
    }
    
}
